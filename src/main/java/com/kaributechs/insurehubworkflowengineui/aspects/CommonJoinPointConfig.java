package com.kaributechs.insurehubworkflowengineui.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class CommonJoinPointConfig {
    @Pointcut("execution(* com.kaributechs.insurehubworkflowengineui.services.camunda.CamundaProcessesImp.startClaimsProcess(..))")
    public void requestClaim() {
    }

    @Pointcut("execution(* com.kaributechs.insurehubworkflowengineui.services.camunda.CamundaTasksImp.completeTask(..))")
    public void completeTask() {
    }
}


