package com.kaributechs.insurehubworkflowengineui.controllers;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.Claim;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.services.ICamundaProcessServices;
import com.kaributechs.insurehubworkflowengineui.services.claims.IClaimService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Claims Process API endpoints", description = "Endpoints for the claims process only")
//@SecurityRequirement(name = "api")
@CrossOrigin
public class ClaimProcessController {

    Logger logger= LoggerFactory.getLogger(this.getClass());
    final ICamundaProcessServices camundaProcessServices;
    final IClaimService claimService;


    public ClaimProcessController(ICamundaProcessServices camundaProcessServices, IClaimService claimService) {
        this.camundaProcessServices = camundaProcessServices;
        this.claimService = claimService;
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/request-claim")
    @Operation(summary = "Lodge a claim")
    public ProcessInstanceDTO startClaimsProcess(@ModelAttribute @Valid ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO) throws IOException {
        logger.info("Claim request : {}",claimsWorkflowRequestDTO);
        return claimService.startClaimsProcess(claimsWorkflowRequestDTO);
    }

    @RolesAllowed({"admin","user"})
    @GetMapping("/my-claims")
    @Operation(summary = "Get a list of your claims")
    public List<Claim> getMyClaims(){
        return claimService.getMyClaimsImp();
    }

    @GetMapping("/claim/{processId}/evidence")
    @Operation(summary = "Get evidence for a claim using process id(claim id)")
    public List<DocumentDTO> getClaimEvidence(@PathVariable String processId) throws IOException {
        return claimService.getEvidence(processId);
    }

//    @GetMapping("/test")
//    public Map<String, String> test() {
//        Map<String,String> team=new HashMap<>();
//        team.put("Test","Tevdcjdac");
//        team.put("Testdsc","Tdsfdsevdcjdac");
//        team.put("Tesst","Tevsdcdcjdac");
//        team.put("Tesddt","Tevdcjdac");
//
//        return team;
//    }


}
