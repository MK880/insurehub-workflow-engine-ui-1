package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.ICompanyVehicleInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/company-vehicle-insurance")
@Tag(name = "Business/Company vehicle insurance API endpoints", description = "Endpoints for business/company vehicle insurance only")
@Slf4j
public class CompanyVehicleInsuranceController {
    private final ICompanyVehicleInsuranceService companyVehicleInsuranceService;

    public CompanyVehicleInsuranceController(ICompanyVehicleInsuranceService companyVehicleInsuranceService) {
        this.companyVehicleInsuranceService = companyVehicleInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for business/company insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO){
        return companyVehicleInsuranceService.apply(companyVehicleInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get business/company insurance details by process id")
    public CompanyVehicleInsurance getInsuranceByProcessId(@PathVariable String processId){
        return companyVehicleInsuranceService.getInsuranceByProcessId(processId);
    }
}
