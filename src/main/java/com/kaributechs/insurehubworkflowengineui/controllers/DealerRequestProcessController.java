package com.kaributechs.insurehubworkflowengineui.controllers;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.TaskDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.CompleteDealerTaskDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.RequestDealerVariablesDTO;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerProcessService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;

import static org.elasticsearch.transport.TransportChannel.logger;

@RestController
@RequestMapping("/api/v1/dealer-request")
@Tag(name = "Find A Dealer Process API endpoints", description = "Endpoints for the find a dealer process only")
@Slf4j
public class DealerRequestProcessController {
    private final IDealerProcessService dealerProcessService;

    public DealerRequestProcessController(IDealerProcessService dealerProcessService) {
        this.dealerProcessService = dealerProcessService;
    }

    @PostMapping("/request-quote")
    @Operation(summary = "Request for a quotation")
    public ProcessInstanceDTO requestQuoteProcess(@ModelAttribute RequestDealerVariablesDTO requestDealerVariablesDTO) throws IOException {
        logger.info("Dealer request data : {} ",requestDealerVariablesDTO);
        return dealerProcessService.startDealerProcessImp(requestDealerVariablesDTO);
    }

    @RolesAllowed({"admin","user"})
    @PostMapping("/task/{taskId}/complete")
    @Operation(summary = "Complete task using task id")
    public TaskDTO completeTask(@PathVariable String taskId, @RequestBody CompleteDealerTaskDTO completeDealerTaskDTO){
        return dealerProcessService.completeDealerTask(taskId, completeDealerTaskDTO);
    }

}
