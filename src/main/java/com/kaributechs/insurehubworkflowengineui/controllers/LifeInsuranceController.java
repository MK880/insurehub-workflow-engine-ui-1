package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.ILifeInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/life-insurance")
@Tag(name = "Life insurance API endpoints", description = "Endpoints for life insurance only")
@Slf4j
public class LifeInsuranceController {
    private final ILifeInsuranceService lifeInsuranceService;

    public LifeInsuranceController(ILifeInsuranceService lifeInsuranceService) {
        this.lifeInsuranceService = lifeInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for life insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid LifeInsuranceDTO lifeInsuranceDTO){
        return lifeInsuranceService.apply(lifeInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get life insurance details by process id")
    public LifeInsurance getInsuranceByProcessId(@PathVariable String processId){
        return lifeInsuranceService.getInsuranceByProcessId(processId);
    }
}
