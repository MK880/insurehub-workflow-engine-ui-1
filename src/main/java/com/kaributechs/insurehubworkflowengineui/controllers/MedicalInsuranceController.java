package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.IMedicalInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/medical-insurance")
@Tag(name = "Health insurance API endpoints", description = "Endpoints for health insurance only")
@Slf4j
public class MedicalInsuranceController {
    private final IMedicalInsuranceService medicalInsuranceService;

    public MedicalInsuranceController(IMedicalInsuranceService medicalInsuranceService) {
        this.medicalInsuranceService = medicalInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for health insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid MedicalInsuranceDTO medicalInsuranceDTO){
        return medicalInsuranceService.apply(medicalInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get health insurance details by process id")
    public MedicalInsurance getInsuranceByProcessId(@PathVariable String processId){
        return medicalInsuranceService.getInsuranceByProcessId(processId);
    }
}
