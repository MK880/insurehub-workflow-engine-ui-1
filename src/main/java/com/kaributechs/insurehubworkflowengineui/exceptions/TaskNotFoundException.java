package com.kaributechs.insurehubworkflowengineui.exceptions;

public class TaskNotFoundException extends RuntimeException {
    private final String processId;
    public TaskNotFoundException(String taskId) {
        super(String.format("Task with id %s not found.", taskId));
        this.processId = taskId;
    }

    public String getProcessId() {
        return processId;
    }
}
