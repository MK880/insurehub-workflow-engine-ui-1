package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AddEvidenceDTO {

    private String processId;
    private Long clientId;
    private String clientFirstName;
    private String clientLastName;
    private String clientIdNumber;
    private String clientType;

    private String businessName;
    private String businessRegistrationNumber;
    private String documentSourceChannel;


    private MultipartFile[] policeReport;
    private MultipartFile[] videoEvidence;
    private MultipartFile[] audioEvidence;
    private MultipartFile[] additionalDocuments;


}
