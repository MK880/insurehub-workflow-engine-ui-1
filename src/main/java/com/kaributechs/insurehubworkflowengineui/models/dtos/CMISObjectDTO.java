package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CMISObjectDTO {
    private String id;
    private String name;
    private  String type;
}
