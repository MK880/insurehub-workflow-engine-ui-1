package com.kaributechs.insurehubworkflowengineui.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessInstanceDTO {
    String id;

    String name;

    Date startDate;

    String initiator;

    String businessKey;

    ProcessInstanceDTO.ProcessInstanceStatus Status;

    String ProcessDefinitionId;

    String ProcessDefinitionKey;

    String ParentId;

    Integer ProcessDefinitionVersion;

    public static enum ProcessInstanceStatus {
        CREATED,
        RUNNING,
        SUSPENDED,
        CANCELLED,
        COMPLETED;

        private ProcessInstanceStatus() {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public ProcessInstanceStatus getStatus() {
        return Status;
    }

    public void setStatus(ProcessInstanceStatus status) {
        Status = status;
    }

    public String getProcessDefinitionId() {
        return ProcessDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        ProcessDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionKey() {
        return ProcessDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        ProcessDefinitionKey = processDefinitionKey;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public Integer getProcessDefinitionVersion() {
        return ProcessDefinitionVersion;
    }

    public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
        ProcessDefinitionVersion = processDefinitionVersion;
    }
}
