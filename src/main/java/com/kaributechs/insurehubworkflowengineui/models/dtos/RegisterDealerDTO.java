package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegisterDealerDTO {

    private String username;
    @NotNull(message="companyName cannot be null")
    private String companyName;
    private String mobileNumber;
    private String email;
    private boolean available;
}
