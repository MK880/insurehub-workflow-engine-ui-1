package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Claim {

    private String id;
    private String username;
    private String processInstanceId;
    private Date claimCreationDate;
    private Date lastClaimStatusChangeDate;
    private String status;
    private Long companyId;
    private boolean deleted;


    ClaimProcessDetailsDTO processDetails;

    ClaimClientDetailsDTO clientDetails;

    ClaimInsuredDetailsDTO insuredDetails;

    ClaimVehicleDetailsDTO vehicleDetails;

    ClaimDriverDetailsDTO driverDetails;

    ClaimAccidentDetailsDTO accidentDetails;
}
