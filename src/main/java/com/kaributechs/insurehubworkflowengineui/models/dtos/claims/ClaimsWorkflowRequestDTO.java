package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class ClaimsWorkflowRequestDTO {
    private boolean acceptDeclaration;
    private Date claimCreationDate;

    @NotNull(message="insuranceType cannot be null")
    private String insuranceType;

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    private Long clientId;
    private String clientFirstName;
    private String clientLastName;

    @NotNull(message="Personal details cannot be null")
    private String clientEmail;
    private String clientPhoneNumber;
    private String clientIdNumber;
    private String clientType;

    private String insuredId;
    private String insuredSalutation;
    private String insuredFirstName;
    private String insuredLastName;
    private String insuredPhoneNumber;
    private String insuredEmail;
    private String insuredResidentialStreetAddress;
    private String insuredSuburb;
    private String insuredResidentialCity;
    private String insuredRegion;
    private String insuredCountry;
    private String insuredZipCode;
    private String insuredWorkingStreetAddress;
    private String insuredWorkingSuburb;
    private String insuredWorkingCity;
    private String insuredWorkingRegion;
    private String insuredWorkingCountry;
    private String insuredWorkingZipCode;

    private String vehicleMake;
    private String vehicleModel;
    private String vehicleModelYear;
    private String vehicleRegistrationNumber;
    private String vehicleMileage;
    private String vehicleUsage;

    private String driversFirstName;
    private String driversLastName;
    private String driversEmail;
    private String driversPhoneNumber;
    private String driversResidentialStreetAddress;
    private String driversSuburb;
    private String driversResidentialCity;
    private String driversRegion;
    private String driversCountry;
    private String driversZipCode;
    private String driversWorkingStreetAddress;
    private String driversWorkingSuburb;
    private String driversWorkingCity;
    private String driversWorkingRegion;
    private String driversWorkingCountry;
    private String driversWorkingZipCode;
    private String driversLicenseNumber;
    private Date dateOfLicenseIssue;

    private String driversCondition;
    private String accidentHistory;
    private String accidentHistoryDetails;
    private String driversConditionDetails;
    private String vehiclePurpose;
    private String natureOfInjuries;
    private String accidentDescription;

    private String documentSourceChannel;
    private String businessName;
    private String businessRegistrationNumber;

    private MultipartFile[] policeReport;
    private MultipartFile[] videoEvidence;
    private MultipartFile[] audioEvidence;
    private MultipartFile[] additionalDocuments;

}
