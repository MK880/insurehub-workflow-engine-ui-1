package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompleteClaimTaskDTO {
    private boolean approved;
    private boolean autoAssess;
    private boolean getMoreDetails;
    private boolean getPanelBeater;
    private String claimStatus;

    @Override
    public String toString() {
        return "CompleteClaimTaskDTO{" +
                "approved=" + approved +
                ", autoAssess=" + autoAssess +
                ", getMoreDetails=" + getMoreDetails +
                ", getPanelBeater=" + getPanelBeater +
                ", claimStatus='" + claimStatus + '\'' +
                '}';
    }
}
