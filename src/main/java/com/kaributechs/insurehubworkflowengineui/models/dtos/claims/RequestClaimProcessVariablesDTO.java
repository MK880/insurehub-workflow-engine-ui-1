package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestClaimProcessVariablesDTO {

    ClaimProcessDetailsDTO processDetails;

    Date claimCreationDate;
    Long companyId;
    public boolean acceptDeclaration;
    public String insuranceType;

    ClaimClientDetailsDTO clientDetails;
    ClaimInsuredDetailsDTO insuredDetails;
    ClaimVehicleDetailsDTO vehicleDetails;
    ClaimDriverDetailsDTO driverDetails;
    ClaimAccidentDetailsDTO accidentDetails;
    ClaimEvidenceDetailsDTO evidenceDetails;
    public String documentSourceChannel;


}
