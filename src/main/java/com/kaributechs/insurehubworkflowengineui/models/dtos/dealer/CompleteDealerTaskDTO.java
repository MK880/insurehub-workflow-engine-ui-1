package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompleteDealerTaskDTO {
    private boolean fixedCar;
    private boolean towedCar;
    private boolean contactedDealer;
    private boolean cancelledRequest;
}
