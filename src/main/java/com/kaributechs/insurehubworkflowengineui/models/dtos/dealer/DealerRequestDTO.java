package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.Data;

import java.util.List;


@Data
public class DealerRequestDTO {

    private Long id;
    private boolean accident;
    private boolean towing;
    private String processId;

    private ServiceDTO requestedService;

    private DriverDetailsDTO driverDetails;

    private ServiceDetailsDTO serviceDetails;

    private VehicleDetailsDTO vehicleDetails;

    private List<QuotationDTO> quotations;

    private boolean fixedCar;
    private boolean towedCar;
    private boolean contactedDealer;
    private boolean cancelledRequest;

}
