package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class DriverDetailsDTO {
    private String driverNationalID;
    private String driverSalutation;
    private String driverLastName;
    private String driverFirstName;
    private String driverEmail;
    private String driverPhoneNumber;
}
