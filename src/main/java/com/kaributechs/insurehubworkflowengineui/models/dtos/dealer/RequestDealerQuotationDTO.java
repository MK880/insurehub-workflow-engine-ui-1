package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDealerQuotationDTO {
    private boolean accident;
    private boolean towing;
    private Long requestedServiceId;

    private DriverDetailsDTO driverDetails;
    private ServiceDetailsDTO serviceDetails;
    private VehicleDetailsDTO vehicleDetails;


}
