package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class ServiceDetailsDTO {
    private String location;
    private String serviceType;
    private String preferredServiceDate;
    private String furtherRequests;
    private String vehicleImage;

}
