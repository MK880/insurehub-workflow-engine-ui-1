package com.kaributechs.insurehubworkflowengineui.models.dtos.enums;

public enum TaskStatus {
    CLAIMED,NOT_CLAIMED
}
