package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business;

import lombok.Data;

@Data
public class ContactDetails {

    private Long id;
    private String emailAddress;
    private String phoneNumber;
    private String residentialStreetAddress;
    private String residentialCity;
    private String region;
    private String residentialZip;
    private String workAddressZip;
    private String country;
    private String workStreetAddress;
    private String workCity;
    private String zip;
    private String workRegion;
    private String workCountry;
    private String suburb;
    private String workSuburb;
}
