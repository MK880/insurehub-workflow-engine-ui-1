package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;


import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import lombok.Data;

import java.util.List;


@Data
public class CompanyVehicleInsurance {

    private Long id;

    private CompanyDetails companyDetails;


    private ContactDetails contactDetails;


    private List<NewBusinessVehicleDetails> vehicles;


    private CompanyInsuranceQuestions insuranceQuestions;


}
