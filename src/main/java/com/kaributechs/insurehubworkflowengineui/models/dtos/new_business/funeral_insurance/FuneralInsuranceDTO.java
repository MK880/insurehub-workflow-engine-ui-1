package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance;


import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetailsDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.SpouseDetailsDTO;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class FuneralInsuranceDTO {

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @Hidden
    private String processId;

    @NotNull(message = "memberDetails cannot be null")
    private FuneralInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message = "contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    private SpouseDetailsDTO spouse;


    private Set<FuneralInsuranceChildrenDetailsDTO> children;


    private Set<FuneralInsuranceExtendedFamilyDetailsDTO> extendedFamily;


    private Set<FuneralInsuranceBeneficiaryDetailsDTO> beneficiaries;

    @NotNull(message = "insuranceQuestions cannot be null")
    private FuneralInsuranceQuestionsDTO insuranceQuestions;
}
