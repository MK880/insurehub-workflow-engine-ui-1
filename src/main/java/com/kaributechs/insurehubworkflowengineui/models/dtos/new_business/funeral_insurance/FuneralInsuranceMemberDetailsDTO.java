package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance;

import lombok.Data;

import java.util.Date;

@Data
public class FuneralInsuranceMemberDetailsDTO {

    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String gender;
    private String MaritalStatus;
    private String IdentificationType;
}
