package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetailsDTO;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;


@Data
public class HouseholdInsuranceDTO {

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @Hidden
    private String processId;

    @NotNull(message = "memberDetails cannot be null")
    private HouseholdInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message = "contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;


    @Valid
    private Set<HouseholdInsurancePropertyDetailsDTO> properties;


    private Set<HouseholdInsuranceChildrenDetailsDTO> children;


    private Set<HouseholdInsuranceExtendedFamilyDetailsDTO> extendedFamily;


    private Set<HouseholdInsuranceBeneficiaryDetailsDTO> beneficiaries;

    @NotNull(message = "insuranceQuestions cannot be null")
    private HouseholdInsuranceQuestionsDTO insuranceQuestions;
}
