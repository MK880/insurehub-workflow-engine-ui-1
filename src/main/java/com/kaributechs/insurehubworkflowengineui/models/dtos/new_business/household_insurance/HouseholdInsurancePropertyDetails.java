package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import lombok.Data;

@Data
public class HouseholdInsurancePropertyDetails {

    private Long id;
    private boolean isOccuppiedByFamilyOnly;
    private String occupationDescription;
    private String propertyUse;
    private String isLetToAFamily;
    private String numberOfTenants;
    private String isBuiltWithBricks;
    private String materialsUsedToBuild;
    private String isRoofedWithSlateOrTiles;
    private String materialsUsedToRoof;
    private String propertyHasPreservationOrder;
    private String preservationOrderDescription;
    private String propertyHasUniqueFeatures;
    private String uniqueFeatures;
    private String yearHouseWasBuilt;
    private String areaOfProperty;
    private String areaOfPropertyDetails;


    private HouseholdInsurancePropertyAddress propertyAddress;


    private HouseholdInsuranceProtectionDetails protectionDetails;



}
