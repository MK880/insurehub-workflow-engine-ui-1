package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance;

import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.SpouseDetails;
import lombok.Data;

import java.util.Set;

@Data
public class LifeInsurance {

    private Long id;

    private String processId;



    private LifeInsuranceMemberDetails memberDetails;

    private ContactDetails contactDetails;


    private SpouseDetails spouse;


    private Set<LifeInsuranceBeneficiaryDetails> beneficiaries;


    private Set<LifeInsuranceExtendedFamilyDetails> extendedFamily;


    private LifeInsuranceQuestions insuranceQuestions;


    private OtherInsuranceDetails otherInsuranceDetails;


}
