package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance;

import lombok.Data;

import java.util.Date;

@Data
public class LifeInsuranceBeneficiaryDetails {

    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String maritalStatus;
    private String identificationType;
    private String relationshipToMember;
    private String proceeds;


}
