package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;



import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.SpouseDetails;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class MedicalInsurance {


    private Long id;

    private String processId;

    private MedicalInsuranceMemberDetails memberDetails;


    private ContactDetails contactDetails;


    private SpouseDetails spouse;


    private Set<MedicalInsuranceChildrenDetails> children;


    private Set<MedicalInsuranceExtendedFamilyDetails> extendedFamily;


    private Set<MedicalInsuranceBeneficiaryDetails> beneficiaries;

    private MedicalInsuranceQuestions insuranceQuestions;

}
