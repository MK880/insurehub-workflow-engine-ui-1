package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;



import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetailsDTO;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class MedicalInsuranceDTO {

    @NotNull(message="companyId cannot be null")
    private Long companyId;

    @Hidden
    private String processId;

    @NotNull(message="memberDetails cannot be null")
    private MedicalInsuranceMemberDetailsDTO memberDetails;

    @NotNull(message="contactDetails cannot be null")
    private ContactDetailsDTO contactDetails;

    private SpouseDetailsDTO spouse;

    private Set<MedicalInsuranceChildrenDetailsDTO> children;

    private Set<MedicalInsuranceExtendedFamilyDetailsDTO> extendedFamily;

    private Set<MedicalInsuranceBeneficiaryDetailsDTO> beneficiaries;

    @NotNull(message="insuranceQuestions cannot be null")
    private MedicalInsuranceQuestionsDTO insuranceQuestions;

}
