package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class MedicalInsuranceMemberDetails  {

    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String gender;
    private String maritalStatus;
    private String identificationType;
}
