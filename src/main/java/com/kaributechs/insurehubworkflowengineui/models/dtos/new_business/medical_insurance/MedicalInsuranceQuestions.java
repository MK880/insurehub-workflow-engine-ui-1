package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class MedicalInsuranceQuestions {

    private Long id;
    private String insuranceType;
    private String insurancePremiumRange;
    private String paymentFrequency;
    private String insuranceCurrency;
    private String condolenceFee;
    private String excessYesOrNo;
    private String extendedFamilyBenefits;
    private String periodOfInsuranceFrom;
    private String maritalStatus;
}
