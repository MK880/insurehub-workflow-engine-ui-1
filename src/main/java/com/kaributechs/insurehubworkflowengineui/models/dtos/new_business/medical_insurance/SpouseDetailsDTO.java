package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance;

import lombok.Data;

import java.util.Date;

@Data
public class SpouseDetailsDTO {
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String relationshipToMember;
    private String identificationType;
    private String funeralCover;
}
