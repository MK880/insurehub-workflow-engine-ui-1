package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance;

import lombok.Data;

@Data
public class PersonalInsuranceSecondaryDriverDetailsDTO {
    private boolean isVehicleUsedByOthers;
    private String dependentLicenseNumber;
    private String dependentDateWhenLicenseWasObtained;
    private String dependentNationalId;
    private String relationshipToBeneficiary;
    private String dependentFirstName;
    private String dependentLastName;
    private String dependentEmailAddress;
    private String dependentDateOfBirth;
    private String dependentPhoneNumber;
    private String dependentGender;
    private String SecDriverIdentificationType;
}
