package com.kaributechs.insurehubworkflowengineui.services;


import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class EvidenceServiceImp implements IEvidenceService {


    private final Session session;
    private final Utils utils;
    private final FoldersServiceImp foldersServiceImp;

    public EvidenceServiceImp(Session session, Utils utils, FoldersServiceImp foldersServiceImp) {
        this.session = session;
        this.utils = utils;
        this.foldersServiceImp = foldersServiceImp;
    }

    @Override
    public List<DocumentDTO> uploadEvidenceImp(EvidenceDTO evidenceDTO) throws IOException {
//        logger.info(evidenceDTO.toString());
        List<DocumentDTO> uploadedDocuments = new ArrayList<>();
        for (MultipartFile file :
                evidenceDTO.getFiles()) {

            byte[] bytes = file.getBytes();

            InputStream inputStream = new ByteArrayInputStream(bytes);
            ContentStream contentStream = session.getObjectFactory().createContentStream(file.getOriginalFilename(), file.getSize(), file.getContentType(), inputStream);
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

            Map<String, Object> properties = new HashMap<>();
            properties.put(PropertyIds.OBJECT_TYPE_ID, "D:ih:evidence");
            properties.put(PropertyIds.NAME, FilenameUtils.removeExtension(file.getOriginalFilename()).concat("_" + timeStamp + "." + FilenameUtils.getExtension(file.getOriginalFilename())));
           properties.put(PropertyIds.CONTENT_STREAM_MIME_TYPE,"cmis:contentStreamMimeType");
            properties.put("ih:original-filename", file.getOriginalFilename());
            properties.put("ih:processId", evidenceDTO.getProcessId());
            properties.put("ih:client-id", evidenceDTO.getClientId());
            properties.put("ih:client-first-name", evidenceDTO.getClientFirstName());
            properties.put("ih:client-last-name", evidenceDTO.getClientLastName());
            properties.put("ih:client-id-number", evidenceDTO.getClientIdNumber());
            UUID uuid = UUID.randomUUID();
            properties.put("ih:document-unique-id",uuid.toString());

            properties.put("ih:business-name",evidenceDTO.getBusinessName());
            properties.put("ih:business-registration-number",evidenceDTO.getBusinessRegistrationNumber());
            properties.put("ih:category",evidenceDTO.getCategory());
            properties.put("ih:client-type",evidenceDTO.getClientType());
            properties.put("ih:document-certified","");
            properties.put("ih:document-source-channel",evidenceDTO.getDocumentSourceChannel());
            properties.put("ih:document-type",file.getContentType());
            properties.put("ih:document-uploader",utils.getUsername());

            try {
                Document doc = getEvidenceFolder().createDocument(properties, contentStream, VersioningState.MAJOR);
                String fileDownloadUri = ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path("/api/v1/evidence/download/")
                        .path(StringUtils.substringBefore(doc.getId(), ";"))
                        .toUriString();
                uploadedDocuments.add(new DocumentDTO(StringUtils.substringBefore(doc.getId(), ";"),
                        doc.getPropertyValue("ih:original-filename"),
                        doc.getType().getDisplayName(),
                        doc.getPropertyValue("ih:category"),
                        doc.getPropertyValue("ih:client-id"),
                        doc.getPropertyValue("ih:client-first-name"),
                        doc.getPropertyValue("ih:client-last-name"),
                        doc.getPropertyValue("ih:client-id-number"),
                        doc.getPropertyValue("ih:processId"),
                        doc.getPropertyValue("ih:document-unique-id"),
                        doc.getPaths().get(0),
                        fileDownloadUri,
                        doc.getContentStream().getStream().readAllBytes()));
            } catch (Exception e) {

                log.error("Error While uploading Document : " + file.getOriginalFilename() + e.getMessage());
//                throw new GenericException("Error While adding Document : " + e.getMessage());
            }
        }

        for (int i = 0; i <uploadedDocuments.size(); i++) {
            if (i>0){
                uploadedDocuments.get(i).setData(null);
            }
        }
        return uploadedDocuments;


    }

    @Override
    public DocumentDTO getEvidenceByIdImp(String id) throws IOException {
        CmisObject object = session.getObject(id);
        if (object instanceof Document) {
            Document document = (Document) object;
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/api/v1/evidence/download/")
                    .path(StringUtils.substringBefore(document.getId(), ";"))
                    .toUriString();

            return new DocumentDTO(StringUtils.substringBefore(document.getId(), ";"),
                    document.getPropertyValue("ih:original-filename"),
                    document.getType().getDisplayName(),
                    document.getPropertyValue("ih:category"),
                    document.getPropertyValue("ih:client-id"),
                    document.getPropertyValue("ih:client-first-name"),
                    document.getPropertyValue("ih:client-last-name"),
                    document.getPropertyValue("ih:client-id-number"),
                    document.getPropertyValue("ih:processId"),
                    document.getPropertyValue("ih:document-unique-id"),
                    document.getPaths().get(0),
                    fileDownloadUri,
                    document.getContentStream().getStream().readAllBytes());
        } else throw new GenericException("InsureHub Folder not found : ");
    }

    @Override
    public ResponseEntity<byte[]> downloadEvidenceImp(String id) throws IOException {
        Document document = (Document) session.getObject(id);
        String filename = document.getProperty("ih:original-filename").getValueAsString();
        ContentStream cs = document.getContentStream(null);
        BufferedInputStream in = new BufferedInputStream(cs.getStream());
        File file = utils.convertInputStreamToFile(in, filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
                .body(FileUtils.readFileToByteArray(file));
    }

    @Override
    public List<DocumentDTO> getEvidenceByClaimIdImp(String processId) throws IOException {
        List<DocumentDTO> documents = new ArrayList<>();

        String query = String.format("SELECT * FROM ih:evidence where ih:processId = \'%s\' ", processId);
        ItemIterable<QueryResult> queryResult = session.query(query, false);
        for (QueryResult item : queryResult) {

            if (!((Boolean) item.getPropertyByQueryName("ih:deleted").getFirstValue())) {
                documents.add(getEvidenceByIdImp((String) item.getPropertyByQueryName("cmis:objectId").getFirstValue()));
            }
        }

        for (int i = 0; i <documents.size(); i++) {
            if (i>0){
                documents.get(i).setData(null);
            }
        }

        return documents;
    }

    @Override
    public List<DocumentDTO> addEvidenceImp(AddEvidenceDTO addEvidenceDTO) throws IOException {
        MultipartFile[] policeReport= addEvidenceDTO.getPoliceReport();
        MultipartFile[] videoEvidence=addEvidenceDTO.getVideoEvidence();
        MultipartFile[] audioEvidence=addEvidenceDTO.getAudioEvidence();
        MultipartFile[] additionalDocuments=addEvidenceDTO.getAdditionalDocuments();

        if(policeReport!=null && policeReport.length>0){
            uploadEvidenceImp(mapEvidenceData(addEvidenceDTO,"Police Report", addEvidenceDTO.getProcessId(), policeReport));
        }
        if(videoEvidence!=null && videoEvidence.length>0){
            uploadEvidenceImp(mapEvidenceData(addEvidenceDTO,"Video Evidence", addEvidenceDTO.getProcessId(), videoEvidence));
        }
        if(audioEvidence!=null && audioEvidence.length>0){
            uploadEvidenceImp(mapEvidenceData(addEvidenceDTO,"Audio Evidence", addEvidenceDTO.getProcessId(), audioEvidence));
        }
        if(additionalDocuments!=null && additionalDocuments.length>0){
            uploadEvidenceImp(mapEvidenceData(addEvidenceDTO,"Additional Documents", addEvidenceDTO.getProcessId(), additionalDocuments));
        }

        return null;
    }

    private EvidenceDTO mapEvidenceData(AddEvidenceDTO addEvidenceDTO, String category, String processId, MultipartFile[] files){
        EvidenceDTO evidenceDTO=new EvidenceDTO();
        evidenceDTO.setProcessId(processId);
        evidenceDTO.setClientId(addEvidenceDTO.getClientId());
        evidenceDTO.setClientFirstName(addEvidenceDTO.getClientFirstName());
        evidenceDTO.setClientLastName(addEvidenceDTO.getClientLastName());
        evidenceDTO.setFiles(files);
        evidenceDTO.setClientIdNumber(addEvidenceDTO.getClientIdNumber());
        evidenceDTO.setDocumentSourceChannel(addEvidenceDTO.getDocumentSourceChannel());
        evidenceDTO.setBusinessName(addEvidenceDTO.getBusinessName());
        evidenceDTO.setBusinessRegistrationNumber(addEvidenceDTO.getBusinessRegistrationNumber());
        evidenceDTO.setCategory(category);
        evidenceDTO.setClientType(addEvidenceDTO.getClientType());
        return evidenceDTO;
    }

    @Override
    public void deleteEvidenceImp(String id) {
        Document document = (Document) session.getObject(id);
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("ih:deleted",true);
        document.updateProperties(properties);
    }

    private Folder getInsureHubFolder() {
        try {
            String path = session.getRootFolder().getPath().concat("InsureHub");
            CmisObject object = session.getObjectByPath(path);
            if (object instanceof Folder) {
                return (Folder) object;
            } else throw new GenericException("InsureHub Folder not found : ");
        } catch (CmisObjectNotFoundException e) {
            FolderDTO folderDTO = new FolderDTO();
            folderDTO.setName("InsureHub");

            return (Folder) session.getObject(foldersServiceImp.createFolderImp(folderDTO).getId());
        }


    }

    private Folder getEvidenceFolder() {
        try {
            String path = session.getRootFolder().getPath().concat("InsureHub/Claims/Evidence");
            CmisObject object = session.getObjectByPath(path);
            if (object instanceof Folder) {
                return (Folder) object;
            }
            else throw new GenericException("Something went wrong whilst getting Evidence Folder : ");
        } catch (CmisObjectNotFoundException e) {
            FolderDTO folderDTO = new FolderDTO();
            folderDTO.setName("InsureHub");
            Map<String, String> newFolderProps = new HashMap<String, String>();
            newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");

            return (Folder) session.getObject(session.createPath(null,"/InsureHub/Claims/Evidence",newFolderProps).getId());
        }


    }
}
