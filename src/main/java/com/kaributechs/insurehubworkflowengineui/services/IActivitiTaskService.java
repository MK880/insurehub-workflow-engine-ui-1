package com.kaributechs.insurehubworkflowengineui.services;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.RequestClaimProcessVariablesDTO;

import java.util.List;

public interface IActivitiTaskService {
    List<TaskResponseDTO> getMyTasksImp();

    List<TaskDTO> getProcessTasksImp(String processInstanceId);

    List<TaskDTO> getTasksByUsernameImp(String username);

    TaskResponseDTO getTaskById(String taskId);

    TaskDTO completeTask(String taskId, RequestClaimProcessVariablesDTO requestClaimProcessVariablesDTO);

    ResponseDTO claimTask(String taskId);

    ResponseDTO assignTask(AssignTaskDTO assignTaskDTO);
}
