package com.kaributechs.insurehubworkflowengineui.services;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.RequestClaimProcessVariablesDTO;

import java.io.IOException;
import java.util.List;

public interface ICamundaProcessServices {
    List<ProcessDefinitionDTO> getAllProcessesImp();

    List<ProcessInstanceDTO> getRunningProcessesImp();

    ResponseDTO suspendProcessImp(String processId);

    ResponseDTO activateProcessImp(String processId);

    ResponseDTO stopProcessByIdImp(String processInstanceId);

    ResponseDTO resumeProcessByIdImp(String processInstanceId);

    RequestClaimProcessVariablesDTO getProcessVariablesImp(String processInstanceId);

    String[] getCurrentTaskImp(String processInstanceId);

}
