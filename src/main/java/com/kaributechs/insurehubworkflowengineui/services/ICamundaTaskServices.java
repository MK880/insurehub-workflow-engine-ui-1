package com.kaributechs.insurehubworkflowengineui.services;

import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.CompleteClaimTaskDTO;

import java.util.List;

public interface ICamundaTaskServices {
    List<TaskDTO> getProcessTasksImp(String processInstanceId);

    TaskDTO completeTask(String taskId, CompleteClaimTaskDTO completeClaimTaskDTO);

    TaskResponseDTO getTaskById(String taskId);

    List<TaskDTO> getTasksByUsernameImp(String username);

    ResponseDTO assignTask(AssignTaskDTO assignTaskDTO);

    ResponseDTO claimTask(String taskId);

    List<TaskDTO> getMyTasksImp();

    List<TaskDTO> getTeamTasksImp();
}
