//package com.kaributechs.insurehubworkflowengineui.services.activiti;
//
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
//import com.kaributechs.insurehubworkflowengineui.services.IActivitiProcessServices;
//import com.kaributechs.insurehubworkflowengineui.utilities.Constants;
//import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
//import org.apache.tomcat.util.codec.binary.Base64;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.http.*;
//import org.springframework.stereotype.Service;
//import org.springframework.util.Base64Utils;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactor.core.publisher.Mono;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//
//
//@Service
//public class ActivitiProcessesImp implements IActivitiProcessServices {
//
//    Logger logger = LoggerFactory.getLogger(this.getClass());
//    @Qualifier("activitiWebClient")
//    private final WebClient activitiWebClient;
//    private final Utils utils;
//
//    public ActivitiProcessesImp(WebClient activitiWebClient, Utils utils) {
//        this.activitiWebClient = activitiWebClient;
//        this.utils = utils;
//    }
//
//    @Override
//    public List<ProcessDefinitionDTO> getAllProcessesImp() {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ProcessDefinitionDTO[]> response = activitiWebClient.get().uri("/processes").accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(ProcessDefinitionDTO[].class).log();
//
//        ProcessDefinitionDTO[] processes = response.block();
//        logger.info(processes.toString());
//        return Arrays.asList(processes.clone());
//    }
//
//    @Override
//    public List<ProcessInstanceDTO> getRunningProcessesImp() {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ProcessInstanceDTO[]> response = activitiWebClient.get().uri("/processes/running").accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(ProcessInstanceDTO[].class).log();
//
//        ProcessInstanceDTO[] processes = response.block();
////        logger.info(processes.toString());
//        return Arrays.asList(processes.clone());
//    }
//
//
//    @Override
//    public ResponseDTO suspendProcessImp(String processId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ResponseDTO> response = activitiWebClient.post().uri("/process/{processId}/suspend", processId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .exchangeToMono(clientResponse -> {
//                            if (clientResponse.statusCode().isError()) {
//                                logger.info(clientResponse.toString());
//                                return null;
//                            }
//                                return clientResponse.bodyToMono(ResponseDTO.class);
//
//                        }
//
//
//                );
//
//        ResponseDTO responseDTO = response.block();
//        return responseDTO;
//    }
//
//    @Override
//    public ResponseDTO activateProcessImp(String processId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ResponseDTO> response = activitiWebClient.post().uri("/process/{processId}/activate", processId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .exchangeToMono(clientResponse -> {
//                            if (clientResponse.statusCode().isError()) {
//                                logger.info(clientResponse.toString());
//                                return null;
//                            }
//                            return clientResponse.bodyToMono(ResponseDTO.class);
//
//                        }
//
//
//                );
//
//        return response.block();
//    }
//
//    @Override
//    public ResponseDTO stopProcessByIdImp(String processInstanceId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ResponseDTO> response = activitiWebClient.post().uri("/process/{processInstanceId}/stop", processInstanceId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .exchangeToMono(clientResponse -> {
//                            if (clientResponse.statusCode().isError()) {
//                                logger.info(clientResponse.toString());
//                                return null;
//                            }
//                            return clientResponse.bodyToMono(ResponseDTO.class);
//
//                        }
//
//
//                );
//
//        return response.block();
//    }
//
//    @Override
//    public ResponseDTO resumeProcessByIdImp(String processInstanceId) {
////        Todo :Implement activiti resume process
////        Mono<ResponseDTO> response = activitiWebClient.post().uri("/process/{processInstanceId}/resume", processInstanceId).accept(MediaType.APPLICATION_JSON)
////                .header(HttpHeaders.AUTHORIZATION)
////                .exchangeToMono(clientResponse -> {
////                            if (clientResponse.statusCode().isError()) {
////                                logger.info(clientResponse.toString());
////                                return null;
////                            }
////                            return clientResponse.bodyToMono(ResponseDTO.class);
////
////                        }
////
////
////                );
////
////        return response.block();
//        return null;
//    }
//
//    @Override
//    public ProcessInstanceDTO startProcessByIdImp(String processId, ClaimsWorkflowRequest claimsWorkflowRequest) {
////        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
////
////        Mono<ProcessInstanceDTO> response=activitiWebClient.post().uri("/processes/running").accept(MediaType.APPLICATION_JSON)
////                .header(HttpHeaders.AUTHORIZATION,basicAuthHeader)
////                .body()
////                .retrieve()
////                .bodyToMono(ProcessInstanceDTO.class).log();
////
////        ProcessDefinitionDTO[] processes=response.block();
////        logger.info(processes.toString());
//
//        return null;
//    }
//
//
//    @Override
//    public ProcessInstanceDTO startClaimsProcess(ClaimsWorkflowRequest claimsWorkflowRequest) throws IOException {
//
////        ObjectMapper oMapper = new ObjectMapper();
////        Map<String, Object> map = oMapper.convertValue(claimsWorkflowRequest, Map.class);
////
////        System.out.println(map);
////        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
////
////        Mono<ProcessInstanceDTO> response = activitiWebClient.post().uri("/request-claim")  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
////                .body(BodyInserters.fromFormData(map))
////                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
////                .retrieve()
////                .bodyToMono(ProcessInstanceDTO.class);
////
////        return response.block();
//        String serverUrl = Constants.activitiEngineBaseUrl+"/request-claim";
//
//        RestTemplate restTemplate = new RestTemplate();
//        String plainCreds = "manager:password";
//        byte[] plainCredsBytes = plainCreds.getBytes();
//        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
//        String base64Creds = new String(base64CredsBytes);
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//        headers.add("Authorization", "Basic " + base64Creds);
//
//
//        ObjectMapper oMapper = new ObjectMapper();
//        oMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
//
//
//        FileSystemResource[] files = new FileSystemResource[claimsWorkflowRequest.getEvidence().length];
//        int i = 0;
//        for (MultipartFile evidence : claimsWorkflowRequest.getEvidence()) {
//
//            files[i] = new FileSystemResource(utils.convertMultipartFileToFile(evidence));
//            i++;
//        }
//
//
//
//        claimsWorkflowRequest.setEvidence(null);
//        MultiValueMap<String, Object> map = oMapper.convertValue(claimsWorkflowRequest, LinkedMultiValueMap.class);
//        map.remove("evidence");
//        map.add("evidence", files);
//        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
//
//        ResponseEntity<ProcessInstanceDTO> response = restTemplate.exchange( serverUrl, HttpMethod.POST,request , ProcessInstanceDTO.class );
//        return  response.getBody();
//    }
//
//}
