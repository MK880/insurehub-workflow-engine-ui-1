//package com.kaributechs.insurehubworkflowengineui.services.activiti;
//
//import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
//import com.kaributechs.insurehubworkflowengineui.services.IActivitiTaskService;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Service;
//import org.springframework.util.Base64Utils;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactor.core.publisher.Mono;
//
//import java.util.Arrays;
//import java.util.List;
//
//@Service
//public class ActivitiTasksImp implements IActivitiTaskService {
//    private final WebClient activitiWebClient;
//
//    public ActivitiTasksImp(WebClient activitiWebClient) {
//        this.activitiWebClient = activitiWebClient;
//    }
//
//    @Override
//    public List<TaskResponseDTO> getMyTasksImp() {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<TaskResponseDTO[]> response = activitiWebClient.get().uri("/tasks").accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(TaskResponseDTO[].class);
//
//        TaskResponseDTO[] tasks = response.block();
//        return Arrays.asList(tasks.clone());
//    }
//
//    @Override
//    public List<TaskDTO> getProcessTasksImp(String processInstanceId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<TaskDTO[]> response = activitiWebClient.get().uri("/{processInstanceId}/tasks", processInstanceId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(TaskDTO[].class);
//
//        TaskDTO[] tasks = response.block();
//        return Arrays.asList(tasks.clone());
//    }
//
//
//    @Override
//    public List<TaskDTO> getTasksByUsernameImp(String username) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<TaskDTO[]> response = activitiWebClient.get().uri("/user-tasks/{username}", username).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(TaskDTO[].class);
//
//        TaskDTO[] tasks = response.block();
//        return Arrays.asList(tasks.clone());
//    }
//
//    @Override
//    public TaskResponseDTO getTaskById(String taskId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<TaskResponseDTO> response = activitiWebClient.get().uri("/task/{taskId}", taskId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(TaskResponseDTO.class);
//
//        return response.block();
//    }
//
//    @Override
//    public TaskDTO completeTask(String taskId, RequestClaimProcessVariablesDTO requestClaimProcessVariablesDTO) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<TaskDTO> response = activitiWebClient.post().uri("/task/{taskId}/complete", taskId).accept(MediaType.APPLICATION_JSON).body(Mono.just(requestClaimProcessVariablesDTO), RequestClaimProcessVariablesDTO.class)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(TaskDTO.class);
//
//        return response.block();
//    }
//
//    @Override
//    public ResponseDTO claimTask(String taskId) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ResponseDTO> response = activitiWebClient.post().uri("/task/{taskId}/claim", taskId).accept(MediaType.APPLICATION_JSON)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(ResponseDTO.class);
//
//        return response.block();
//    }
//
//    @Override
//    public ResponseDTO assignTask(AssignTaskDTO assignTaskDTO) {
//        String basicAuthHeader = "basic " + Base64Utils.encodeToString(("manager" + ":" + "password").getBytes());
//
//        Mono<ResponseDTO> response = activitiWebClient.post().uri("/task/assign").accept(MediaType.APPLICATION_JSON).body(Mono.just(assignTaskDTO), AssignTaskDTO.class)
//                .header(HttpHeaders.AUTHORIZATION, basicAuthHeader)
//                .retrieve()
//                .bodyToMono(ResponseDTO.class);
//
//        return response.block();
//    }
//}
