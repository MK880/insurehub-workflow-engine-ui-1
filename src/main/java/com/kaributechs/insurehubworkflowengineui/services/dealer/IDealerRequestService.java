package com.kaributechs.insurehubworkflowengineui.services.dealer;

import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerRequestDTO;

public interface IDealerRequestService {
    DealerRequestDTO getDealerRequestByProcessId(String processId);
}
