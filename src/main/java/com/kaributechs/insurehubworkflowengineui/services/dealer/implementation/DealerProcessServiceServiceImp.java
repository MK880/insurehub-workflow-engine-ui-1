package com.kaributechs.insurehubworkflowengineui.services.dealer.implementation;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.*;
//import com.kaributechs.insurehubworkflowengineui.services.IEvidenceService;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerProcessService;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
@Slf4j
public class DealerProcessServiceServiceImp implements IDealerProcessService {
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
//    private final IEvidenceService evidenceService;

//    public DealerProcessServiceServiceImp(Utils utils, WebClient dbWebClient, WebClient camundaWebClient, IEvidenceService evidenceService) {
//        this.utils = utils;
//        this.dbWebClient = dbWebClient;
//        this.camundaWebClient = camundaWebClient;
//        this.evidenceService = evidenceService;
//    }


    public DealerProcessServiceServiceImp(Utils utils, WebClient dbWebClient, WebClient camundaWebClient) {
        this.utils = utils;
        this.dbWebClient = dbWebClient;
        this.camundaWebClient = camundaWebClient;
    }

    @Override
    public ProcessInstanceDTO startDealerProcessImp(RequestDealerVariablesDTO requestDealerVariablesDTO) throws IOException {
        log.info("Start dealer process : {}",requestDealerVariablesDTO);
        MultipartFile vehicleImage= requestDealerVariablesDTO.getVehicleImage();
        ProcessInstanceDTO processInstanceDTO=sendDealerDataToCamundaEngine(requestDealerVariablesDTO);

        if (processInstanceDTO!=null){
            String vehicleImageUrl = null;
            if(vehicleImage!=null && !requestDealerVariablesDTO.getVehicleImage().isEmpty()){
                vehicleImageUrl=utils.uploadFileToDB(processInstanceDTO.getId(),"vehicle image",utils.convertMultipartFileToFile(requestDealerVariablesDTO.getVehicleImage()));
            } else log.info("No vehicle image supplied");
            log.info("Uploaded vehicle image url : {}",vehicleImageUrl);
            RequestDealerQuotationDTO requestDealerQuotationDTO = mapToRequestDealerDTO(requestDealerVariablesDTO);
            requestDealerQuotationDTO.getServiceDetails().setVehicleImage(vehicleImageUrl);

            DealerRequestDTO dealerRequestDTO=mapToDealerRequestDTO(requestDealerVariablesDTO);
            dealerRequestDTO.setProcessId(processInstanceDTO.getId());

        }else  log.error("Something went wrong whilst creating process");

        log.info("Dealer process created : {}",processInstanceDTO);
        return  processInstanceDTO;
    }


    public ProcessInstanceDTO sendDealerDataToCamundaEngine(RequestDealerVariablesDTO requestDealerVariablesDTO) {

        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/dealer/request-quote")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(mapToRequestDealerDTO(requestDealerVariablesDTO)), RequestDealerQuotationDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);

            return response.block();
        } catch (Exception e){
            log.error("Error creating claim");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    private RequestDealerQuotationDTO mapToRequestDealerDTO(RequestDealerVariablesDTO requestDealerVariablesDTO){
        RequestDealerQuotationDTO requestDealerQuotationDTO = new RequestDealerQuotationDTO();
        BeanUtils.copyProperties(requestDealerVariablesDTO, requestDealerQuotationDTO);

        DriverDetailsDTO driverDetailsDTO=new DriverDetailsDTO();
        VehicleDetailsDTO vehicleDetailsDTO=new VehicleDetailsDTO();
        ServiceDetailsDTO serviceDetailsDTO=new ServiceDetailsDTO();

        BeanUtils.copyProperties(requestDealerVariablesDTO,driverDetailsDTO);
        BeanUtils.copyProperties(requestDealerVariablesDTO,vehicleDetailsDTO);
        BeanUtils.copyProperties(requestDealerVariablesDTO,serviceDetailsDTO);

        requestDealerQuotationDTO.setServiceDetails(serviceDetailsDTO);
        requestDealerQuotationDTO.setDriverDetails(driverDetailsDTO);
        requestDealerQuotationDTO.setVehicleDetails(vehicleDetailsDTO);
        return requestDealerQuotationDTO;
    }

    private DealerRequestDTO mapToDealerRequestDTO(RequestDealerVariablesDTO requestDealerVariablesDTO){
        DealerRequestDTO dealerRequestDTO=new DealerRequestDTO();
        BeanUtils.copyProperties(requestDealerVariablesDTO,dealerRequestDTO);

        DriverDetailsDTO driverDetailsDTO=new DriverDetailsDTO();
        VehicleDetailsDTO vehicleDetailsDTO=new VehicleDetailsDTO();
        ServiceDetailsDTO serviceDetailsDTO=new ServiceDetailsDTO();

        BeanUtils.copyProperties(requestDealerVariablesDTO,driverDetailsDTO);
        BeanUtils.copyProperties(requestDealerVariablesDTO,vehicleDetailsDTO);
        BeanUtils.copyProperties(requestDealerVariablesDTO,serviceDetailsDTO);

        dealerRequestDTO.setServiceDetails(serviceDetailsDTO);
        dealerRequestDTO.setDriverDetails(driverDetailsDTO);
        dealerRequestDTO.setVehicleDetails(vehicleDetailsDTO);
        return dealerRequestDTO;
    }

    @Override
    public TaskDTO completeDealerTask(String taskId, CompleteDealerTaskDTO completeDealerTaskDTO) {
        try {
            Mono<TaskDTO> response = camundaWebClient.post().uri("/dealer/task/{taskId}/complete",taskId)
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(completeDealerTaskDTO), CompleteDealerTaskDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(TaskDTO.class);
            return response.block();
        } catch (Exception e){
            log.error("Error completing dealer request");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }
    }
}
