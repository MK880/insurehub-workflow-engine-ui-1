package com.kaributechs.insurehubworkflowengineui.services.dealer.implementation;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerRequestDTO;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerRequestService;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class DealerRequestServiceImp implements IDealerRequestService {
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;
    private final Utils utils;

    public DealerRequestServiceImp(WebClient dbWebClient, Utils utils) {
        this.dbWebClient = dbWebClient;
        this.utils = utils;
    }


    @Override
    public DealerRequestDTO getDealerRequestByProcessId(String processId) {
        log.info("Getting Dealer Request for process with id : {}",processId);

        try {
            Mono<DealerRequestDTO> response = dbWebClient.get().uri("/dealer-request/process/{processId}", processId)
                    .accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(DealerRequestDTO.class);

            return response.block();
        } catch (Exception e) {
            throw new GenericException("Error registering dealer :" + e.getMessage());
        }    }
}
