package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class FuneralInsuranceServiceImp implements IFuneralInsuranceService{
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public FuneralInsuranceServiceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(FuneralInsuranceDTO funeralInsuranceDTO) {
        log.info("Start funeral insurance process : {}",funeralInsuranceDTO);

        ProcessInstanceDTO processInstanceDTO= sendFuneralInsuranceDataToCamundaEngine(funeralInsuranceDTO);

        if (processInstanceDTO!=null){
            funeralInsuranceDTO.setProcessId(processInstanceDTO.getId());

            saveFuneralInsuranceDataToDBEngine(funeralInsuranceDTO);

        }
        log.info("company funeral insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;    }

    @Override
    public FuneralInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<FuneralInsurance> response = dbWebClient.get().uri("/funeral-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve().bodyToMono(FuneralInsurance.class);
            return response.block();

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendFuneralInsuranceDataToCamundaEngine(FuneralInsuranceDTO funeralInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/funeral-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(funeralInsuranceDTO), FuneralInsuranceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);
            return response.block();
        } catch (Exception e){
            log.error("Error requesting funeral insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }
    }

    public void saveFuneralInsuranceDataToDBEngine(FuneralInsuranceDTO funeralInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/funeral-insurance/save").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(funeralInsuranceDTO), FuneralInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();
            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error creating funeral insurance : Status code " + status.value());
            }
        } catch (Exception e) {
            throw new GenericException("Error creating funeral insurance :" + e.getMessage());
        }

    }


}
