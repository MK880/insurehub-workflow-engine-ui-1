package com.kaributechs.insurehubworkflowengineui.services.new_business;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance.CompanyVehicleInsuranceDTO;

public interface ICompanyVehicleInsuranceService {

    ProcessInstanceDTO apply(CompanyVehicleInsuranceDTO companyVehicleInsuranceDTO);

    CompanyVehicleInsurance getInsuranceByProcessId(String processId);
}
