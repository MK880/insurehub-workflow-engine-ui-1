package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance.HouseholdInsuranceDTO;

public interface IHouseholdInsuranceService {
    ProcessInstanceDTO apply(HouseholdInsuranceDTO householdInsuranceDTO);

    HouseholdInsurance getInsuranceByProcessId(String processId);
}
