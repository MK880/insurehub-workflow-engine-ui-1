package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;

public interface ILifeInsuranceService {
    ProcessInstanceDTO apply(LifeInsuranceDTO lifeInsuranceDTO);
    LifeInsurance getInsuranceByProcessId(String processId);
}
