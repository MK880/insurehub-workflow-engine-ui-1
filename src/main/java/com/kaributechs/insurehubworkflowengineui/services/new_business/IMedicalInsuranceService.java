package com.kaributechs.insurehubworkflowengineui.services.new_business;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsuranceDTO;

public interface IMedicalInsuranceService {
    ProcessInstanceDTO apply(MedicalInsuranceDTO medicalInsuranceDTO);

    MedicalInsurance getInsuranceByProcessId(String processId);
}
