package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.medical_insurance.MedicalInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class IMedicalInsuranceServiceImp implements IMedicalInsuranceService {
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public IMedicalInsuranceServiceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(MedicalInsuranceDTO medicalInsuranceDTO) {
        log.info("Start Medical Insurance process : {}",medicalInsuranceDTO);

        ProcessInstanceDTO processInstanceDTO= sendMedicalInsuranceDataToCamundaEngine(medicalInsuranceDTO);

        if (processInstanceDTO!=null){
            medicalInsuranceDTO.setProcessId(processInstanceDTO.getId());

            saveMedicalInsuranceDataToDBEngine(medicalInsuranceDTO);

        }
        log.info("medical insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;
    }

    @Override
    public MedicalInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<MedicalInsurance> response = dbWebClient.get().uri("/medical-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .exchangeToMono(clientResponse -> {
                                if (clientResponse.statusCode().isError()) {
                                    return null;
                                }
                                return clientResponse.bodyToMono(MedicalInsurance.class);
                            }
                    );
            return response.block();

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendMedicalInsuranceDataToCamundaEngine(MedicalInsuranceDTO medicalInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/medical-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(medicalInsuranceDTO), MedicalInsuranceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);

            return response.block();
        } catch (Exception e){
            log.error("Error requesting medical insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }

    }

    public void saveMedicalInsuranceDataToDBEngine(MedicalInsuranceDTO medicalInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/medical-insurance/save").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(medicalInsuranceDTO), MedicalInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();


            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error Creating medical insurance : Status code " + status.value());
            }


        } catch (Exception e) {
            throw new GenericException("Error Creating Medical Insurance :" + e.getMessage());
        }

    }

}
