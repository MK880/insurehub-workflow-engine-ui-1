package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;

public interface IPersonalVehicleInsuranceService {
    ProcessInstanceDTO apply(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO);

    PersonalVehicleInsurance getInsuranceByProcessId(String processId);
}
