package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class LifeInsuranceServiceImp implements ILifeInsuranceService{
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public LifeInsuranceServiceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(LifeInsuranceDTO lifeInsuranceDTO) {
        log.info("Start Life Insurance process : {}",lifeInsuranceDTO);

        ProcessInstanceDTO processInstanceDTO= sendLifeInsuranceDataToCamundaEngine(lifeInsuranceDTO);

        if (processInstanceDTO!=null){
            lifeInsuranceDTO.setProcessId(processInstanceDTO.getId());

            saveLifeInsuranceDataToDBEngine(lifeInsuranceDTO);

        }
        log.info("life insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;    }

    @Override
    public LifeInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<LifeInsurance> response = dbWebClient.get().uri("/life-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve().bodyToMono(LifeInsurance.class);
            return response.block();

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendLifeInsuranceDataToCamundaEngine(LifeInsuranceDTO lifeInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/life-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(lifeInsuranceDTO), LifeInsuranceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);
            return response.block();
        } catch (Exception e){
            log.error("Error requesting life insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }
    }

    public void saveLifeInsuranceDataToDBEngine(LifeInsuranceDTO lifeInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/life-insurance/save").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(lifeInsuranceDTO), LifeInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();
            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error creating life insurance : Status code " + status.value());
            }
        } catch (Exception e) {
            throw new GenericException("Error creating life insurance :" + e.getMessage());
        }

    }


}
