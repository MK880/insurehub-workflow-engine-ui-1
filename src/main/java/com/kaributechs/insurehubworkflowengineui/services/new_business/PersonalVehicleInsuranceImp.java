package com.kaributechs.insurehubworkflowengineui.services.new_business;


import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.utilities.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class PersonalVehicleInsuranceImp implements IPersonalVehicleInsuranceService {
    private static final String AUTHORIZATION="Authorization";
    private static final String BEARER="Bearer ";
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;
    private final Utils utils;
    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;

    public PersonalVehicleInsuranceImp(WebClient camundaWebClient, Utils utils, WebClient dbWebClient) {
        this.camundaWebClient = camundaWebClient;
        this.utils = utils;
        this.dbWebClient = dbWebClient;
    }

    @Override
    public ProcessInstanceDTO apply(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO) {
        log.info("Start Personal Vehicle Insurance process : {}",personalVehicleInsuranceDTO);
        ProcessInstanceDTO processInstanceDTO=sendPersonalVehicleInsuranceDataToCamundaEngine(personalVehicleInsuranceDTO);

        if (processInstanceDTO!=null){
            personalVehicleInsuranceDTO.setProcessId(processInstanceDTO.getId());
            savePersonalVehicleInsuranceDataToDBEngine(personalVehicleInsuranceDTO);
        }
        log.info("personal vehicle insurance process created : {}",processInstanceDTO);
        return  processInstanceDTO;
    }

    @Override
    public PersonalVehicleInsurance getInsuranceByProcessId(String processId) {
        try {
            Mono<PersonalVehicleInsurance> response = dbWebClient.get().uri("/personal-vehicle-insurance/get/{processId}", processId).accept(APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION,AUTHORIZATION, BEARER+utils.getToken())
                    .retrieve().bodyToMono(PersonalVehicleInsurance.class);
            return response.block();

        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    public ProcessInstanceDTO sendPersonalVehicleInsuranceDataToCamundaEngine(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO) {
        try {
            Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/personal-vehicle-insurance/apply")
                    .accept(MediaType.APPLICATION_JSON).body(Mono.just(personalVehicleInsuranceDTO), PersonalVehicleInsuranceDTO.class)
                    .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+utils.getToken())
                    .retrieve()
                    .bodyToMono(ProcessInstanceDTO.class);
            return response.block();
        } catch (Exception e){
            log.error("Error requesting personal vehicle insurance");
            throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
        }
    }

    public void savePersonalVehicleInsuranceDataToDBEngine(PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/personal-vehicle-insurance/apply").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + utils.getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(personalVehicleInsuranceDTO), PersonalVehicleInsuranceDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();
            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error creating personal vehicle insurance : Status code " + status.value());
            }
        } catch (Exception e) {
            throw new GenericException("Error creating personal vehicle insurance :" + e.getMessage());
        }

    }


}
