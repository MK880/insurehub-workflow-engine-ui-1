package com.kaributechs.insurehubworkflowengineui.utilities;


import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.*;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.DBWorkflowRequestDTO;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Service
public class Utils {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Qualifier("dbWebClient")
    private final WebClient dbWebClient;
    @Qualifier("camundaWebClient")
    private final WebClient camundaWebClient;

    public Utils(WebClient dbWebClient, WebClient camundaWebClient) {
        this.dbWebClient = dbWebClient;
        this.camundaWebClient = camundaWebClient;
    }


//    public void getUserCredentials(){
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String username = ((UserDetails)principal).getUsername();
//        String password = ((UserDetails)principal).getPassword();
//
//        logger.info(username +" "+password);
//    }

    public File convertMultipartFileToFile(MultipartFile file) throws IOException {
        String tDir = System.getProperty("java.io.tmpdir");
        File convFile = new File(tDir + "/" + file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public File byteToFile(String filename, byte[] bytes) {
        try {
            String tDir = System.getProperty("java.io.tmpdir");
            File convFile = new File(tDir + "/" + filename);
            FileUtils.writeByteArrayToFile(convFile, bytes);
            return convFile;
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        }

    }


    public String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof KeycloakPrincipal) {
            KeycloakPrincipal principal = (KeycloakPrincipal) authentication.getPrincipal();
            AccessToken accessToken = principal.getKeycloakSecurityContext().getToken();
            return accessToken.getPreferredUsername();
        } else return null;
    }

    public String getToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof KeycloakPrincipal) {
            KeycloakPrincipal principal = (KeycloakPrincipal) authentication.getPrincipal();
            return principal.getKeycloakSecurityContext().getTokenString();
        } else return null;
    }

    public void saveClaimToDBEngine(ClaimDTO claimDTO) {
        try {
            Mono<ClientResponse> response = dbWebClient.post().uri("/claim/add").accept(MediaType.APPLICATION_JSON)
                    .header(HttpHeaders.AUTHORIZATION, "Authorization", "Bearer " + getToken())
                    .body(BodyInserters.fromPublisher(Mono.just(claimDTO), ClaimDTO.class))
                    .exchange();
            HttpStatus status = Objects.requireNonNull(response.block()).statusCode();


            if (!status.is2xxSuccessful()) {
                throw new GenericException("Error Creating Claim : Status code " + status.value());
            }


        } catch (Exception e) {
            throw new GenericException("Error Creating Claim :" + e.getMessage());
        }

    }

    public ProcessInstanceDTO sendClaimDataToCamundaEngine(DBWorkflowRequestDTO dbWorkflowRequestDTO) {
            try {
                Mono<ProcessInstanceDTO> response = camundaWebClient.post().uri("/request-claim")
                        .accept(MediaType.APPLICATION_JSON).body(Mono.just(dbWorkflowRequestDTO), ClaimsWorkflowRequestDTO.class)
                        .header(HttpHeaders.AUTHORIZATION,"Authorization", "Bearer "+getToken())
                        .retrieve()
                        .bodyToMono(ProcessInstanceDTO.class);

                return response.block();
            } catch (Exception e){
                logger.error("Error creating claim : {}",e.getMessage() );
                throw new GenericException("Error From Camunda Engine :"+ e.getMessage());
            }

        }

    public void getUserAttributes() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getPrincipal() instanceof KeycloakPrincipal) {

            KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) authentication.getPrincipal();
            AccessToken token = kp.getKeycloakSecurityContext().getToken();
            Map<String, Object> otherClaims = token.getOtherClaims();
            logger.info(otherClaims.toString());
        }
    }

    public File convertInputStreamToFile(BufferedInputStream file, String name) throws IOException
    {
        String tDir = System.getProperty("java.io.tmpdir");
        File convFile = new File(tDir+"/"+name);


        FileUtils.copyInputStreamToFile(file, convFile);

        return convFile;
    }

    public MultiValueMap<String, Object> objectMap(Object obj) {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            try { map.add(field.getName(), field.get(obj)); } catch (Exception e) {
                logger.error(e.toString());
                logger.error("Something went wrong whilst converting object {} to map",obj.getClass().getSimpleName());
            }
        }
        return map;
    }


    public String uploadFileToDB(String processId, String type, File file) {
        logger.info("Uploading documents to file engine");

        try {
                logger.info("Uploading {}",file.getName());
                FileSystemResource value = new FileSystemResource(file);
                MultiValueMap<String, Object> formData = new LinkedMultiValueMap<>();
                formData.add("processId", processId);
                formData.add("type", type);
                formData.add("files", value);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                HttpEntity<MultiValueMap<String, Object>> requestEntity
                        = new HttpEntity<>(formData, headers);

//                String serverUrl = "http://localhost:6004/api/v1/files/upload";
                String serverUrl = "http://files-engine:6004/api/v1/files/upload";


                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<FileUploadResponseDTO[]> responseDTOList = restTemplate.exchange(serverUrl, HttpMethod.POST, requestEntity, FileUploadResponseDTO[].class);
                List<FileUploadResponseDTO> object = Arrays.asList(responseDTOList.getBody());
                logger.info("{} uploaded successfully",file.getName());
                return object.get(0).getDownloadUrl();

        }catch (Exception e){
            logger.info(e.getMessage());
            return null;

        }
    }


}
